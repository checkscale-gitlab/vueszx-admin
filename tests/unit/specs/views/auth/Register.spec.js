import Component from '@/views/auth/register/Register'
import { vm, setup } from '../../main'

describe('Register', () => {
  beforeEach(() => {
    // clear mock
    jest.resetModules()
    jest.clearAllMocks()

    // setting component
    setup(Component)
  })

  it('onSubmit', async () => {
    await vm.onSubmit()

    vm.$validator.validateAll = () => {
      return new Promise(resolve => {
        resolve(true)
      })
    }
    await vm.onSubmit()
  })
})
