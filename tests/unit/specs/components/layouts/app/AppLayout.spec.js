import Component from '@/components/layouts/app/AppLayout'
import { setup } from '../../../main'

describe('AppLayout', () => {
  beforeEach(() => {
    // clear mock
    jest.resetModules()
    jest.clearAllMocks()

    // setting component
    setup(Component)
  })

  it('init', () => {
    'Nothing to test'
  })
})
