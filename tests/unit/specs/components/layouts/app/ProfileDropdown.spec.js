import Component from '@/components/layouts/app/app-navbar/dropdowns/ProfileDropdown'
import { vm, setup } from '../../../main'

describe('ProfileDropdown', () => {
  beforeEach(() => {
    // clear mock
    jest.resetModules()
    jest.clearAllMocks()

    // setting component
    setup(Component)
  })

  it('clearAuth', () => {
    localStorage.setItem('jwt', 'test')
    vm.clearAuth()
    expect(localStorage.getItem('jwt')).toBeNull()
  })
})
