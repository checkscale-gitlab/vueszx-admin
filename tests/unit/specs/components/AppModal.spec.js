import Component from '@/components/AppModal'
import { wrapper, vm, setup } from '../main'

const bvModalEvt = {
  preventDefault: () => true
}

describe('AppModal', () => {
  beforeEach(() => {
    // clear mock
    jest.resetModules()
    jest.clearAllMocks()

    // setting component
    setup(Component, {
      propsData: {
        app: 'test'
      }
    })
  })

  it('emitCloseAdd', async () => {
    await vm.emitCloseAdd()
    expect(wrapper.emitted('close-add')).toBeTruthy()
  })

  it('emitCloseUpdate', async () => {
    await vm.emitCloseUpdate()
    expect(wrapper.emitted('close-update')).toBeTruthy()
  })

  it('emitSubmitAdd', async () => {
    await vm.emitSubmitAdd(bvModalEvt)
    expect(wrapper.emitted('submit-add')).toBeTruthy()
  })

  it('emitSubmitUpdate', async () => {
    await vm.emitSubmitUpdate(bvModalEvt)
    expect(wrapper.emitted('submit-update')).toBeTruthy()
  })
})
