import Component from '@/components/AppException'
import { setup } from '../main'

describe('AppException', () => {
  beforeEach(() => {
    // clear mock
    jest.resetModules()
    jest.clearAllMocks()

    // setting component
    setup(Component, {
      propsData: {
        number: '404',
        text: 'Test'
      }
    })
  })

  it('init', () => {
    'Noting to test'
  })
})
