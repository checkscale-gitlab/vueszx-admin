// https://docs.cypress.io/api/introduction/api.html

describe('My First Test', () => {
  it('Visits the app root url', () => {
    cy.visitSite('/')
    cy.url().should('include', '/auth/login')
  })
})
