/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import AppWidget from '@/components/AppWidget'
import store from '@/store'
import { text, boolean } from '@storybook/addon-knobs'
import { action } from '@storybook/addon-actions'

export const actionMethods = {
  onClickAdd: action('show add modal'),
  onClickMultiDelete: action('show multi delete modal')
}

storiesOf('Widget', module)
  .add('Using',
    () => ({
      store,
      components: {
        AppWidget
      },
      props: {
        textHeader: {
          default: text('textHeader', 'Stories')
        },
        bgHeader: {
          default: text('bgHeader', 'base')
        },
        colorHeader: {
          default: text('colorHeader', 'white')
        },
        hasAdd: {
          default: boolean('hasAdd', true)
        },
        hasDelete: {
          default: boolean('hasDelete', true)
        }
      },
      mounted () {
        // set store data
        this.$store.commit('setUserPerm', [
          'stories.add',
          'stories.delete'
        ])
      },
      methods: {
        ...actionMethods
      },
      template: `
        <div>
          <b-container>
            <app-widget
              app="stories"
              :textHeader="textHeader"
              :bgHeader="bgHeader"
              :colorHeader="colorHeader"
              :hasAdd="hasAdd"
              :hasDelete="hasDelete"
              @click-add="onClickAdd"
              @click-delete="() => { $confirmDelete([], true); onClickMultiDelete() }"
            >Widget</app-widget>
          </b-container>
        </div>
      `
    }),
    {
      info: '# Documentation'
    }
  )
