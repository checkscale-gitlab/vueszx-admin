/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import AppModal from '@/components/AppModal'
import store from '@/store'
import '@/i18n'
import { action } from '@storybook/addon-actions'

export const actionMethods = {
  onCloseAdd: action('close-add'),
  onCloseUpdate: action('close-update'),
  onSubmitAdd: action('submit-add'),
  onSubmitUpdate: action('submit-update'),
}

storiesOf('Modal Groups', module)
  .add('Using',
    () => ({
      store,
      components: {
        AppModal
      },
      mounted () {
        // set store data
        this.$store.commit('setUserPerm', [
          'stories.add',
          'stories.update'
        ])
      },
      methods: {
        ...actionMethods
      },
      template: `
        <b-container>
          <b-row class="mt-3 justify-content-center">
            <b-col cols="auto">
              <b-button variant="success" @click="$bvModal.show('add-modal')">Add Modal</b-button>
            </b-col>
            <b-col cols="auto">
              <b-button class="text-white" variant="warning" @click="$bvModal.show('update-modal')">Update Modal</b-button>
            </b-col>
          </b-row>

          <app-modal 
            id="app_modal"
            app="stories"
            @close-add="onCloseAdd"
            @close-update="onCloseUpdate"
            @submit-add="onSubmitAdd"
            @submit-update="onSubmitUpdate"
          >
            <template slot="add-modal">
              Add modal
            </template>

            <template slot="update-modal">
              Update modal
            </template>
          </app-modal>
        </b-container>
      `
    }),
    {
      info: '# Documentation'
    }
  )
