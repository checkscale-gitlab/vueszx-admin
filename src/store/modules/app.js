const state = {
  config: {
    locale: 'gb',
    googleMaps: {
      apiKey: 'AIzaSyBNAqPrTQoz9P4NBlDDyfxrnKiafkaL8iQ'
    },
    classToggleSidebar: '',
  },
  isLoading: true,
  userPermission: [
    'dashboard.view',
    'data_axios.view',
    'data_axios.add',
    'data_axios.update',
    'data_axios.delete',
    'data_table.view',
    'data_table.add',
    'data_table.update',
    'data_table.delete',
    'form_data.view',
    'alert.view',
  ],
  alert: {
    show: false,
    status: '',
    type: '',
    title: '',
    text: ''
  },
}

const mutations = {
  setLocale (state, locale) {
    state.config.locale = locale
  },
  setLoading (state, isLoading) {
    state.isLoading = isLoading
  },
  setClassToggleSidebar (state, setClass) {
    state.config.classToggleSidebar = setClass
  },
  setUserPerm (state, userPermission) {
    state.userPermission = userPermission
  },
  setAlert (state, alert) {
    state.alert = alert
  }
}

const actions = {

}

export default {
  state,
  mutations,
  actions
}
