/* import vue */
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import YmapPlugin from 'vue-yandex-maps'
import VeeValidate from 'vee-validate'
import VueSweetalert2 from 'vue-sweetalert2'
import VueApexCharts from 'vue-apexcharts'
import '@/fontawesome'
import { FontAwesomeIcon, FontAwesomeLayers, FontAwesomeLayersText } from '@fortawesome/vue-fontawesome'
import request from '@/util/request'

/* import storybook */
import { configure, addDecorator } from '@storybook/vue'
import { withKnobs } from '@storybook/addon-knobs'
import { setDefaults, withInfo } from 'storybook-addon-vue-info'

/*
  ===== vue =====
*/
Vue.use(BootstrapVue)
Vue.use(YmapPlugin)
Vue.use(VueSweetalert2)
Vue.use(VeeValidate, {
  inject: true,
  fieldsBagName: 'veeFields',
  errorBagName: 'veeErrors'
})

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('font-awesome-layers', FontAwesomeLayers)
Vue.component('font-awesome-layers-text', FontAwesomeLayersText)
Vue.component('apexchart', VueApexCharts)

Vue.mixin({
  methods: {
    $api: request, // request to api with axios
    $validateState (ref) { // validate on change input field
      if (
        this.veeFields[ref] &&
        (this.veeFields[ref].dirty || this.veeFields[ref].validated)
      ) {
        return !this.veeErrors.has(ref)
      }
      return null
    },
    $validateStateFeedback (ref) {
      if (
        this.veeFields[ref] &&
        (this.veeFields[ref].dirty || this.veeFields[ref].validated)
      ) {
        return !this.veeErrors.has(ref) ? null : this.veeErrors.first(ref)
      }
      return null
    },
    $submitDelete (data) { // submit delete should be override in component
      return new Promise((resolve, reject) => {
        resolve(true)
      })
    },
    $confirmDelete (data) { // confirm delete modal
      this.$swal.fire({
        type: 'warning',
        title: this.$t('alert.submitDelete.title'),
        text: this.$t('alert.submitDelete.text'),
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: this.$t('alert.submitDelete.confirmButtonText')
      }).then((result) => {
        if (result.value) {
          if (data.length === 0) {
            // minimum select atleast one
            this.$store.commit('setAlert', {
              show: true,
              status: 'custom',
              type: 'warning',
              title: this.$t('alert.submitDelete.warning.title'),
              text: this.$t('alert.submitDelete.warning.text')
            })
            return
          }

          // call submitDelete to do what it should
          this.$submitDelete(data).then(() => {
            this.$store.commit('setAlert', {
              show: true,
              status: 'custom',
              type: 'success',
              title: this.$t('alert.submitDelete.done.title'),
              text: this.$t('alert.submitDelete.done.text')
            })
          })
        }
      })
    },
  },
})

/*
  ===== storybook =====
*/

addDecorator(withKnobs)
addDecorator(withInfo)
setDefaults({
  header: false
})

const req = require.context('../../src/stories', true, /.stories.js$/)

function loadStories () {
  req.keys().forEach(filename => req(filename))
}

configure(loadStories, module)
